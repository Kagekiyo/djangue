from django.shortcuts import render_to_response,HttpResponse,redirect,render
from django.http import HttpResponse, Http404
from .forms import userForm
from .models import user
from django.http import JsonResponse

def entryPoint(request): # список всех мероприятий
    return render(request,'users.html')

def showUsers(request): # список всех мероприятий
    allUsers = list(user.objects.all().values())
    return JsonResponse(allUsers, safe=False)
def edit(request, user_id):
    try:
        myinstance = user.objects.get(pk=user_id)
    except:
        raise Http404("Такого юзера еще не создали :(")
    if request.POST:
        form = userForm(request.POST, instance=myinstance)
        print(myinstance)
        form.save()
        allUsers = list(user.objects.all().values())
        return JsonResponse(allUsers, safe=False)
    else:
        form = userForm(instance=myinstance)
        return render(request, 'edit.html', {'form': form, 'id':user_id})

def delete(request, user_id):
    try:
        p = user.objects.get(pk=user_id)
        p.delete()
        allUsers = list(user.objects.all().values())
        return JsonResponse(allUsers, safe=False)
    except:
        raise Http404("Такого юзера еще не создали :(")
def add(request):
    if request.POST:
        form = userForm(request.POST)
        if form.is_valid():
            form.save()
        else:
            print('Form is not valid')
        return redirect('/users/showAll')
    else:
        form = userForm()
        return render(request, 'add.html', {'form': form})
def sortAsc(request): # список всех мероприятий
    sort_by = request.GET.get('sort_key')
    allUsers = list(user.objects.order_by(str(sort_by).values()))
    return JsonResponse(allUsers, safe=False)
def sortDesc(request): # список всех мероприятий
    sort_by = request.GET.get('sort_key')
    allUsers = list(user.objects.order_by('-'+str(sort_by)).values())
    return JsonResponse(allUsers, safe=False)
