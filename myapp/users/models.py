from django.db import models
from django.contrib.auth.models import AbstractUser
# Create your models here.
class user(AbstractUser):
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    surname = models.CharField(max_length=30, blank=True)
    name = models.CharField(max_length=30, blank=True) #blank - null или нет
    age = models.IntegerField(default = 0, blank=True)
