from .models import user
from django import forms
class userForm(forms.ModelForm):
    class Meta:
        model = user
        fields = ('username',
                  'first_name',
                  'last_name',
                  'email',
                  'location',
                  'age'
        )
