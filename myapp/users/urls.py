from django.conf.urls import url
from . import views
urlpatterns = [
    url(r'^showAll/$', views.showUsers, name='showusers'),
    url(r'^edit/(?P<user_id>\d+)/$', views.edit),
    url(r'^delete/(?P<user_id>\d+)/$', views.delete),
    url(r'^add/$', views.add),
    url(r'^sortAsc/$', views.sortAsc),
    url(r'^sortDesc/$', views.sortDesc),
    url(r'^entryPoint/$', views.entryPoint),
]
